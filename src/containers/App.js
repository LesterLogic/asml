import { connect } from "react-redux";
import ASMLApp from "../components/ASMLApp.js";
import {
    updateAction
} from "../store/actions/actions.js";

const mapStateToProps = state => {
    return {
        serviceActions: state.serviceActions
    }
};

const mapDispatchToProps = dispatch => {
    return {
        updateAction: (action) => { dispatch(updateAction(action))}
    };
}

const App = connect(
    mapStateToProps,
    mapDispatchToProps
)(ASMLApp);

export default App;