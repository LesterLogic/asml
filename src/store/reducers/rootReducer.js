import { defaultState } from "../store.js";
import {
    UPDATE_ACTION
} from "../actions/actions.js";

export const rootReducer = (state = defaultState, action) => {
    switch (action.type) {
        case UPDATE_ACTION:
            const actions = state.serviceActions.map(act => {
                if (act.action === action.action.action) {
                    return action.action;
                } else {
                    return act;
                }
            });
            return Object.assign({}, state, { serviceActions: actions });
        default:
            return state;
    }
};