export const UPDATE_ACTION = "UPDATE_ACTION";

export function updateAction(action) {
    return { type: UPDATE_ACTION, action: action };
};