export const defaultState = {
    serviceActions: [
        { action: "A", machine: 1, startDate: '2019-03-07T00:00:00', duration: 12, engineers: 2 },
        { action: "B", machine: 2, startDate: '2019-03-08T00:00:00', duration: 24, engineers: 3 },
        { action: "C", machine: 3, startDate: '2019-03-09T00:00:00', duration: 36, engineers: 2 },
        { action: "D", machine: 1, startDate: '2019-03-10T00:00:00', duration: 48, engineers: 5 },
        { action: "E", machine: 2, startDate: '2019-03-11T00:00:00', duration: 72, engineers: 4 }
    ]
};