import React from 'react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

export default function ActionForm(props) {
      return (
        <div>
            <form>
                <Grid container spacing={16}>
                    <Grid item xs={12}>
                        <TextField
                            id="startDate"
                            label="Start Time"
                            type="datetime-local"
                            value={props.action !== null ? props.action.startDate : ''}
                            onChange={props.handleChange}
                            margin="normal"
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            id="duration"
                            label="Duration (hours)"
                            type="number"
                            value={props.action !== null ? props.action.duration : ''}
                            onChange={props.handleChange}
                            margin="normal"
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            id="engineers"
                            label="Engineers:"
                            type="number"
                            value={props.action !== null ? props.action.engineers : ''}
                            onChange={props.handleChange}
                            margin="normal"
                            fullWidth
                        />
                    </Grid>
                </Grid>
            </form>
        </div>
    );
};