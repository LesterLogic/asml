import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import ActionForm from './ActionForm.js';
import Timeline from 'react-visjs-timeline';
import moment from 'moment';
import '../styles/App.css';

class ASMLApp extends Component {
  constructor (props) {
    super(props);
    this.state = { selectedAction: null, dialogOpen: false };

    this.actionSelect = this.actionSelect.bind(this);
    this.handleActionSave = this.handleActionSave.bind(this);
    this.handleActionCancel = this.handleActionCancel.bind(this);
    this.handleActionChange = this.handleActionChange.bind(this);
  }

  actionSelect(e) {
    if (e.items.length > 0) {
    this.setState( { selectedAction: Object.assign({}, this.props.serviceActions[e.items[0]]), dialogOpen: true } );
    } else {
      this.setState( { selectedAction: null, dialogOpen: false } );
    }
  }

  handleActionSave() {
    this.props.updateAction(this.state.selectedAction);
    this.setState( { selectedAction: null, dialogOpen: false } );
  }

  handleActionChange(e) {
    this.setState({selectedAction: {...this.state.selectedAction, [e.target.id]: e.target.value}});
  }

  handleActionCancel() {
    this.setState( { selectedAction: null, dialogOpen: false } );
  }

  render() {
    const items = this.props.serviceActions.map((item, index) => {
      const startTime = moment(item.startDate);
      const endTime = moment(item.startDate).add(item.duration, 'h');
      return {
        content: item.engineers + " engineers",
        end: endTime,
        group: item.machine,
        id: index,
        start: startTime,
        title: `<div>Action: <strong>${item.action}</strong><br />Start: <strong>${startTime.format('MM/DD/YY HH:mm a')}</strong><br />End: <strong>${endTime.format('MM/DD/YY HH:mm a')}</strong><br />Engineers: <strong>${item.engineers}</strong></div>`,
        type: 'range',
        limitSize: false,
        editable: true
      };
    });
    
    const groups = [
      { id: 1, content: 'Machine 1', className: 'item-group' },
      { id: 2, content: 'Machine 2', className: 'item-group' },
      { id: 3, content: 'Machine 3', className: 'item-group' }
    ];
    
    const options = {
      stack: false
    };

    return (
      <main>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography variant="h6" color="inherit">ASML Front-end Assignment &mdash; Jon Lester</Typography>
          </Toolbar>
        </AppBar>
        <div className="timeline">
        <p>Click action to edit. Drag timeline to see more dates. Zoom in/out to change timeline range.</p>
        <Timeline items={items} groups={groups} options={options} selectHandler={this.actionSelect} />
        </div>
        <Dialog
          open={this.state.dialogOpen}
          onClose={this.handleActionCancel}
        >
          <DialogTitle>Editing Action <strong>{this.state.selectedAction != null ? this.state.selectedAction.action : ''}</strong> for machine <strong>{this.state.selectedAction != null ? this.state.selectedAction.machine : ''}</strong></DialogTitle>
          <DialogContent>
            <DialogContentText>Update Start Time, Duration, and number of required Engineers. When finished, click 'Save' or 'Cancel' to abandon changes.</DialogContentText>
            <ActionForm action={this.state.selectedAction} handleChange={this.handleActionChange} />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleActionSave}>Save</Button>
            <Button onClick={this.handleActionCancel}>Cancel</Button>
          </DialogActions>
        </Dialog>
      </main>
    );
  }
}

export default ASMLApp;